package main;

import service.ServiceGame;
import ui.JFrameGame;
import ui.JPanelGame;
import control.ControlGame;
import control.ControlPlayer;
import dto.GameDto;

public class Main {

	public static void main(String[] args){
		//创建游戏数据源
		GameDto dto = new GameDto();
		//创建游戏逻辑模块（连接游戏数据源）
		ServiceGame gameService = new ServiceGame(dto);
		//创建游戏面板
		JPanelGame panelGame = new JPanelGame(dto);
		//创建游戏控制器（连接游戏面板与游戏逻辑模块）
		ControlGame gameControl = new ControlGame(panelGame,gameService);
		//创建玩家控制器（连接游戏控制器）
		ControlPlayer control = new ControlPlayer(gameControl);	
		//安装玩家控制器
		panelGame.setPlayerControl(control);
		//创建游戏窗口，安装游戏面板
		@SuppressWarnings("unused")
		JFrameGame frameGame = new JFrameGame(panelGame);
	}	
}
