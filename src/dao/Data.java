package dao;

import java.util.List;

import dto.Player;


/**
 * 数据持久层接口
 * @author Administrator
 *
 */
public interface Data {

	/**
	 * 读取数据
	 * @return
	 */
	List<Player> loadData();
	
    /*
     * 获取数据
     */
	void saveData(Player players);
	
}
