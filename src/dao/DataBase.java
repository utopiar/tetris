package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import dto.Player;

public class DataBase implements Data {

	private final String dbUrl ;
	private final String dbUser ;
	private final String dbPwd ;
	private static final String LOAD_SQL = "SELECT TOP 5 playerName,playerPoint FROM playerInfo WHERE typeID='1' ORDER BY playerPoint DESC;";
	private static final String SAVE_SQL = "INSERT INTO playerInfo(playerName,playerPoint,typeId) VALUES (?,?,?);";
	
	private Connection connection;
	private PreparedStatement  preparedStatement;
	private ResultSet resultSet;
	
	public DataBase(HashMap<String, String> params){
		dbUrl = params.get("dbUrl");
		dbUser = params.get("dbUser");
		dbPwd = params.get("dbPwd");
		try {
			Class.forName(params.get("driver"));			
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	@Override
	public List<Player> loadData() {

		List<Player> players = new ArrayList<Player>();
		try {
			connection = DriverManager.getConnection(dbUrl, dbUser, dbPwd);
			preparedStatement = connection.prepareStatement(LOAD_SQL);
			resultSet = preparedStatement.executeQuery();
			
			while(resultSet.next()){
				players.add(new Player(resultSet.getString(1), resultSet.getInt(2)));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return players;
	}

	@Override
	public void saveData(Player player) {
		try {
			connection = DriverManager.getConnection(dbUrl, dbUser, dbPwd);
			preparedStatement = connection.prepareStatement(SAVE_SQL);
			preparedStatement.setObject(1, player.getName());
			preparedStatement.setObject(2, player.getPoint());
			preparedStatement.setObject(3, 1);
			preparedStatement.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
