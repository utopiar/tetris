package util;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

import config.ConfigGame;

public class FrameUtil {

	public static void setFrameCenter(JFrame jFrame){
		//居中显示
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension screen = toolkit.getScreenSize();
		int x = screen.width - jFrame.getWidth()>>1;
		int y = (screen.height - jFrame.getHeight()>>1)-ConfigGame.getConfigFrame().getWindowUp();
		jFrame.setLocation(x, y);
	}
}
