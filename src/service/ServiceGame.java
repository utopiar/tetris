package service;

import java.awt.Point;
import java.util.List;
import java.util.Random;

import config.ConfigGame;

import dto.GameDto;
import dto.Player;
import entity.GameAct;

public class ServiceGame {

	private GameDto dto;
	
	/**
	 * 随机数生成器
	 */
	private Random random = new Random();
	
	//TODO 完善加分、升级机制
	/**
	 * 基础加分
	 */
	private static final int BASE_POINT = 2;
	
	/**
	 * 方块最大数量
	 */
	private static final int MAX_TYPE = ConfigGame.getConfigSystem().getMaxType();

	public ServiceGame(GameDto dto) {
		this.dto = dto;
		GameAct act = new GameAct(random.nextInt(MAX_TYPE));
		this.dto.setGameAct(act);
	}

	/**
	 * 控制器方向键（上）
	 */
	public void keyUp() {
		this.dto.getGameAct().round(this.dto.getGameMap());
	}

	/**
	 * 控制器方向键（下）
	 */
	public void keyDown() {
		if(this.dto.getGameAct().move(0, 1,this.dto.getGameMap()))
			return;
		//TODO
		Point[] actPoint = this.dto.getGameAct().getActPoints();
		boolean[][] map = this.dto.getGameMap();
		for (int i = 0; i < actPoint.length; i++) {
			map[actPoint[i].x][actPoint[i].y] = true;
		}
		//刷新一个新的方块
		this.dto.getGameAct().init(this.dto.getNext());
		//创建下一个方块
		this.dto.setNext(random.nextInt(MAX_TYPE));
		//消行、加分、升级
		for (int j = map[0].length-1; j >0; j--) {
			int k = 0;
			for (int i = 0; i < map.length; i++) {
				if(!map[i][j]) {
					break;
				}
				k++;
			}
			System.out.println();
			if(k == map.length){
				int nowRemoveLine = this.dto.getNowRemoveLine();
				int nowPoint = this.dto.getNowPoint();
				int nowLevel = this.dto.getNowLevel();
				nowRemoveLine ++;
				nowPoint += BASE_POINT*(nowLevel+1);
				nowLevel = nowRemoveLine/20;
				this.dto.setNowLevel(nowLevel);
				this.dto.setNowPoint(nowPoint);
				this.dto.setNowRemoveLine(nowRemoveLine);
				for (int l = j; l >0; l--) {
					for (int i = 0; i < map.length; i++) {
						map[i][l] = false;
						map[i][l] = map[i][l-1];
						
					}
				}
				j++;
			}			
		}
		
	}

	/**
	 * 控制器方向键（左）
	 */
	public void keyLeft() {
		this.dto.getGameAct().move(-1, 0,this.dto.getGameMap());
	}

	/**
	 * 控制器方向键（右）
	 */
	public void keyRight() {
		this.dto.getGameAct().move(1, 0,this.dto.getGameMap());
	}
	
	public void keyFunLeft() {
		System.out.println("keyFunLeft");
	}
	public void keyFunRight() {
		System.out.println("keyFunRight");
	}
	
	public void keyFunDown() {
		System.out.println("keyFunDown");
	}

	//TODO---------------------------测试代码------------------------------------------
	public void keyFunUp() {
		int nowLevel = this.dto.getNowLevel();
		int nowRmLine = this.dto.getNowRemoveLine();
		int nowPoint = this.dto.getNowPoint();
		nowPoint +=50;
		nowRmLine +=2;
		nowLevel = nowRmLine/20;
		this.dto.setNowLevel(nowLevel);
		this.dto.setNowPoint(nowPoint);
		this.dto.setNowRemoveLine(nowRmLine);
	}
	
	public void setDbRecode(List<Player> player){
		this.dto.setDbRecode(player);
	}
	
	public void setDiskRecode(List<Player> player){
		this.dto.setDiskRecode(player);
	}
}
