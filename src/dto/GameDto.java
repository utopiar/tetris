package dto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import config.ConfigGame;

import entity.GameAct;

public class GameDto {
	
	private static  final int MAXRECODE = ConfigGame.getConfigSystem().getMaxRecode();

	private List<Player> dbRecode;
	
	private List<Player> diskRecode;
	
	private boolean[][] gameMap;
	
	private GameAct gameAct;
	
	private int next;
	
	private int nowLevel;
	
	private int nowPoint;
	
	private int nowRemoveLine;
	
	public GameDto(){
		dtoInit();
	}
	
	public void dtoInit(){
		this.gameMap = new boolean[ConfigGame.getConfigSystem().getMaxX()+1][ConfigGame.getConfigSystem().getMaxY()+1];
		this.dbRecode = new ArrayList<Player>();
		this.diskRecode = new ArrayList<Player>();
	}

	public List<Player> getDbRecode() {
		return dbRecode;
	}

	public List<Player> setFillRecode(List<Player> players){
		if(players == null){
			players = new ArrayList<Player>();
		}
		while(players.size()<MAXRECODE){
			players.add(new Player("No Data",0));
		}
		Collections.sort(dbRecode);	
		return players;
	}
	
	public void setDbRecode(List<Player> dbRecode) {		
 		this.dbRecode = setFillRecode(dbRecode);
	}

	public List<Player> getDiskRecode() {
		return diskRecode;
	}

	public void setDiskRecode(List<Player> diskRecode) {
		this.diskRecode = setFillRecode(diskRecode);
	}

	public boolean[][] getGameMap() {
		return gameMap;
	}

	public void setGameMap(boolean[][] gameMap) {
		this.gameMap = gameMap;
	}

	public GameAct getGameAct() {
		return gameAct;
	}

	public void setGameAct(GameAct gameAct) {
		this.gameAct = gameAct;
	}

	public int getNext() {
		return next;
	}

	public void setNext(int next) {
		this.next = next;
	}

	public int getNowLevel() {
		return nowLevel;
	}

	public void setNowLevel(int nowLevel) {
		this.nowLevel = nowLevel;
	}

	public int getNowPoint() {
		return nowPoint;
	}

	public void setNowPoint(int nowPoint) {
		this.nowPoint = nowPoint;
	}

	public int getNowRemoveLine() {
		return nowRemoveLine;
	}

	public void setNowRemoveLine(int nowRemoveLine) {
		this.nowRemoveLine = nowRemoveLine;
	}
	
	
}
