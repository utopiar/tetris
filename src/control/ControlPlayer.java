package control;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class ControlPlayer extends KeyAdapter {

	private ControlGame controlGame;
	
	public ControlPlayer(ControlGame controlGame){
		this.controlGame = controlGame;
	}
	
	/**
	 * 响应键盘按下事件
	 * (non-Javadoc)
	 * @see java.awt.event.KeyAdapter#keyPressed(java.awt.event.KeyEvent)
	 */
	
	
	public void keyPressed(KeyEvent e) {
		this.controlGame.actionByKeyCode(e.getKeyCode());
	}
}
