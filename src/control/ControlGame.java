package control;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;

import config.ConfigDataInterface;
import config.ConfigGame;

import service.ServiceGame;
import ui.JPanelGame;
import dao.Data;
import dto.Player;

public class ControlGame {
	
	/**
	 * 画面控制层
	 */
	private JPanelGame panelGame;
	/**
	 * 游戏控制层
	 */
	private ServiceGame gameService;
	
	private final HashMap<Integer, String> actionMap;
	
	private Data dataA;
	private Data dataB;
	
	/**
	 * 控制玩家操作
	 * 控制游戏逻辑
	 * 控制游戏画面
	 * @param panelGame
	 */
	@SuppressWarnings("unchecked")
	public ControlGame(JPanelGame panelGame,ServiceGame gameService){
		this.panelGame = panelGame;
		this.gameService = gameService;
		dataA = getDataInterface(ConfigGame.getConfigData().getCfgDB());
		this.setDbRecode(dataA.loadData());
		dataB = getDataInterface(ConfigGame.getConfigData().getCfgDisk());	
		this.setDiskRecode(dataB.loadData());
		HashMap<String, Integer> tempMap= new HashMap<String, Integer>();
		try {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream("data/control.dat"));
			tempMap = (HashMap<String, Integer>) ois.readObject();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Set<Entry<String, Integer>> entryMap = tempMap.entrySet();
		actionMap = new HashMap<Integer, String>();
		for (Entry<String, Integer> entry : entryMap) {
			actionMap.put(entry.getValue(), entry.getKey());
		}
		
		//硬编码：键盘映射
//		try {
//			actionMap.put(38, this.gameService.getClass().getMethod("keyUp"));
//			actionMap.put(40, this.gameService.getClass().getMethod("keyDown"));
//			actionMap.put(37, this.gameService.getClass().getMethod("keyLeft"));
//			actionMap.put(39, this.gameService.getClass().getMethod("keyRight"));
//			actionMap.put(87, this.gameService.getClass().getMethod("testLevelUp"));
//		} catch (Exception e) {
//			e.printStackTrace();
//		} 
		
	}
	
	/**
	 * 通过反射获得数据接口
	 */
	public Data getDataInterface(ConfigDataInterface cfg){
		try {
			Class<?> cls = Class.forName(cfg.getClassName());
			Constructor<?> ctr = cls.getConstructor(HashMap.class);
			return (Data)ctr.newInstance(cfg.getParams());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void actionByKeyCode(int keyCode) {
		// TODO Auto-generated method stub
		Method actionmMethod;
		try {
			actionmMethod = this.gameService.getClass().getMethod(actionMap.get(keyCode));
			actionmMethod.invoke(this.gameService);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		this.panelGame.repaint();
	}

	//TODO---------------------------测试代码------------------------------------------
	
	public void setDbRecode(List<Player> player){
		this.gameService.setDbRecode(player);
	}
	
	public void setDiskRecode(List<Player> player){
		this.gameService.setDiskRecode(player);
	}

}
