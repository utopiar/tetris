package entity;

import java.awt.Point;

import config.ConfigGame;

public class GameAct {
	
	/**
	 * 游戏X轴方向最小边界
	 */
	private static int MIN_X = ConfigGame.getConfigSystem().getMinX();
	
	/**
	 * 游戏X轴方向最大边界
	 */
	private static int MAX_X = ConfigGame.getConfigSystem().getMaxX();
	
	/**
	 * 游戏Y轴方向最小边界
	 */
	private static int MIN_Y = ConfigGame.getConfigSystem().getMinY();
	
	/**
	 * 游戏Y轴方向最大边界
	 */
	private static int MAX_Y = ConfigGame.getConfigSystem().getMaxY();
	
	/**
	 * 方块类型
	 */
	private int typeCode;
	
	/**
	 * 方块中每一个方格的坐标
	 */
	private Point[] actPoints;
	
	public GameAct(int actCode){
        this.init(actCode);
	}
	
	/**
	 * 初始化方块
	 * @param actCode 方块类型编号
	 */
	public void init(int actCode){
		this.typeCode = actCode;
		Point[] points = ConfigGame.getConfigSystem().getTypeConfig().get(actCode).getPoints();
		actPoints = new Point[points.length];
		for (int i = 0; i < points.length; i++) {
			actPoints[i] = new Point(points[i].x,points[i].y);
		}
	}
	
	public Point[] getActPoints(){
		return actPoints;
	}
	
	/**
	 * 方块移动
	 * @param moveX X轴偏移量
	 * @param moveY Y轴偏移量
	 */
	public boolean move (int moveX,int moveY,boolean[][] map){
		for(Point actPoint : actPoints){
			int newX = actPoint.x + moveX;
			int newY = actPoint.y + moveY;
			if(!isOverZone(newX, newY)||map[newX][newY])
				return false;
		}
		for(Point actPoint : actPoints){
			actPoint.x += moveX;
			actPoint.y += moveY;
		}
		return true;
	}
	
	/**
	 * 顺时针旋转（屏幕直角坐标系下）：
	 * B.x = O.y + O.x - A.y
	 * B.y = O.y - O.x + A.x 
	 */
	public void round(boolean[][] map){
		if (!ConfigGame.getConfigSystem().getTypeConfig().get(typeCode).isTypeRound()) {
			return;
		}
		for (int i = 1; i < actPoints.length; i++) {
			int newX = actPoints[0].y + actPoints[0].x - actPoints[i].y;
			int newY = actPoints[0].y - actPoints[0].x + actPoints[i].x;
			if(!isOverZone(newX, newY)||map[newX][newY])
				return;
		}
		for (int i = 1; i < actPoints.length; i++) {
			int newX = actPoints[0].y + actPoints[0].x - actPoints[i].y;
			int newY = actPoints[0].y - actPoints[0].x + actPoints[i].x;
			actPoints[i].x = newX;
			actPoints[i].y = newY;
		}
	}
	
	/**
	 * 判断是否超出游戏区域
	 * @param newX 新的X轴坐标
	 * @param newY 新的Y轴坐标
	 * @return 返回一个布尔类型
	 */
	public boolean isOverZone(int newX,int newY){
		if(newX<MIN_X||newX>MAX_X||newY<MIN_Y||newY>MAX_Y)
			return false;
		return true;
	}
	
	public int getTypeCode(){
		return typeCode;
	}
}
