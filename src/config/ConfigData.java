package config;

import org.dom4j.Element;

public class ConfigData {

	private ConfigDataInterface cfgDB = null;
	private ConfigDataInterface cfgDisk = null;
	
	@SuppressWarnings("unchecked")
	public ConfigData(Element data){
		Element dataBase = data.element("dataBase");
		Element dataDisk = data.element("dataDisk");
		cfgDB = new ConfigDataInterface(dataBase.attributeValue("className"), dataBase.elements("param"));
		cfgDisk = new ConfigDataInterface(dataDisk.attributeValue("className"), dataDisk.elements("param"));
	}

	public ConfigDataInterface getCfgDB() {
		return cfgDB;
	}

	public ConfigDataInterface getCfgDisk() {
		return cfgDisk;
	}
	
	
}
