package config;

import java.util.ArrayList;
import java.util.List;

import org.dom4j.Element;

public class ConfigSystem {
	
	private final int minX;
	
	private final int maxX;
	
	private final int minY;
	
	private final int maxY;
	
	private final int maxRecode;
	
	private final int maxType;
	
	
	private List<ConfigRect> typeConfig;

	public ConfigSystem(Element system){
		typeConfig = new ArrayList<ConfigRect>();
		minX = Integer.parseInt(system.attributeValue("minX"));
		maxX = Integer.parseInt(system.attributeValue("maxX"));
		minY = Integer.parseInt(system.attributeValue("minY"));
		maxY = Integer.parseInt(system.attributeValue("maxY"));
		maxRecode = Integer.parseInt(system.attributeValue("maxRecode"));
		maxType = Integer.parseInt(system.attributeValue("maxType"));
		@SuppressWarnings("unchecked")
		List<Element> elements = system.elements("rect");
		for (Element element : elements) {
			ConfigRect rect = new ConfigRect(element);
			typeConfig.add(rect);
		}
	}

	public List<ConfigRect> getTypeConfig() {
		return typeConfig;
	}

	public int getMinX() {
		return minX;
	}

	public int getMaxX() {
		return maxX;
	}

	public int getMinY() {
		return minY;
	}

	public int getMaxY() {
		return maxY;
	}

	public int getMaxRecode() {
		return maxRecode;
	}
		
	public int getMaxType() {
		return maxType;
	}
}
