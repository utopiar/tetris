package config;

import java.util.HashMap;
import java.util.List;

import org.dom4j.Element;

public class ConfigDataInterface {

	private String className;
	private HashMap<String, String> params;
	
	public ConfigDataInterface(String className ,List<Element> paramList ){
		this.className = className;
		params = new HashMap<String, String>();
		for (Element element : paramList) {
			params.put(element.attributeValue("key"), element.attributeValue("value"));
		}
	}
	
	public String getClassName() {
		return className;
	}
	public HashMap<String, String> getParams() {
		return params;
	}
}
