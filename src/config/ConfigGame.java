package config;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class ConfigGame {
	
	/**
	 * 窗口配置
	 */
	private static ConfigFrame CFG_F = null;
	/**
	 * 系统配置
	 */
	private static ConfigSystem CFG_S = null;
	/**
	 * 数据访问配置
	 */
	private static ConfigData CFG_D = null;
	static{		
		try {
			
			SAXReader reader = new SAXReader();
			//读取XML文件
			Document doc = reader.read("data/cfg.xml");
			//读取XML文件根节点
			Element game = doc.getRootElement();
			//获取窗口配置参数
			CFG_F = new ConfigFrame(game.element("frame"));
			//获取系统配置参数
			CFG_S = new ConfigSystem(game.element("system"));
			//获取数据访问参数
			CFG_D = new ConfigData(game.element("data"));
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 初始化方法私有化
	 */
	private ConfigGame(){}
	
	public static ConfigFrame getConfigFrame(){
		return CFG_F;
	}
	
	public static ConfigSystem getConfigSystem(){
		return CFG_S;
	}
	
	public static ConfigData getConfigData(){
		return CFG_D;
	}
}
