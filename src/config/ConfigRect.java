package config;

import java.awt.Point;
import java.util.List;

import org.dom4j.Element;

public class ConfigRect {

	/**
	 * 方块是否可以旋转
	 */
	private boolean typeRound;
	
	/**
	 * 方块每个方格坐标
	 */
	private Point[] points;
		
	public ConfigRect(Element element){
		this.typeRound = Boolean.parseBoolean(element.attributeValue("typeRound"));
		@SuppressWarnings("unchecked")
		List<Element> rectElements = element.elements("point");
		points = new Point[rectElements.size()];
		for (int i = 0; i < points.length; i++) {
			int x = Integer.parseInt(rectElements.get(i).attributeValue("x"));
			int y = Integer.parseInt(rectElements.get(i).attributeValue("y"));
			points[i] = new Point(x,y);
		}
	}
	
	public boolean isTypeRound() {
		return typeRound;
	}

	public Point[] getPoints() {
		return points;
	}
	
	
}
