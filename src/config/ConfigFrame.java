package config;

import java.util.ArrayList;
import java.util.List;

import org.dom4j.Element;

public class ConfigFrame {
	
	/**
	 * 游戏窗口标题
	 */
	private String title;
	/**
	 * 窗口上移距离设置
	 */
	private int windowUp;
	/**
	 * 窗口宽度设置
	 */
	private int width;
	/**
	 * 窗口高度设置
	 */
	private int height;
	/**
	 * 窗口相对Frame源点偏移
	 */
	private int padding;
	/**
	 * 窗口内边框宽度
	 */
	private int border;
	/**
	 * 游戏显示层
	 */
	private List<ConfigLayer> layersConfig;
	
	@SuppressWarnings("unchecked")
	public ConfigFrame(Element frame){
		//获取配置文件中的标题
		this.title = frame.attributeValue("title");
		//获取配置文件中的窗口向上偏移值
		this.windowUp = Integer.parseInt(frame.attributeValue("windowUp"));
		//获取配置文件中Frame的宽度
		this.width = Integer.parseInt(frame.attributeValue("width"));
		//获取配置文件中Frame的高度
		this.height = Integer.parseInt(frame.attributeValue("heigth"));
		//获取配置文件中关于窗口相对Frame源点偏移的设定
		this.padding = Integer.parseInt(frame.attributeValue("padding"));
		//获取配置文件中内边框的宽度设定
		this.border = Integer.parseInt(frame.attributeValue("border"));
		List<Element> layers = frame.elements("layer");
		layersConfig = new ArrayList<ConfigLayer>();
		for(Element layer: layers){
			ConfigLayer cl = null;
			cl = new ConfigLayer(
					layer.attributeValue("className"),
					Integer.parseInt(layer.attributeValue("x")),
					Integer.parseInt(layer.attributeValue("y")),
					Integer.parseInt(layer.attributeValue("w")),
					Integer.parseInt(layer.attributeValue("h"))
			);
			layersConfig.add(cl);
		}
	}

	public String getTitle() {
		return title;
	}

	public int getWindowUp() {
		return windowUp;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getPadding() {
		return padding;
	}

	public int getBorder() {
		return border;
	}

	public List<ConfigLayer> getLayersConfig() {
		return layersConfig;
	}

	
}
