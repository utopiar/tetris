package ui;

import java.awt.Graphics;
import java.awt.Image;
import java.util.List;

import dto.Player;

public class LayerData extends Layer {
	
	/*
	 * 显示玩家数量
	 */
	private static final int MAX_PLAYER = 5;
	
	/*
	 * 值槽高度
	 */
	private static final int SLOT_H = 34;
	
	/*
	 * 数据库标题图片高度
	 */
	private static final int DB_H = Img.Database.getHeight(null);
	
	/*
	 * 值槽间的间距
	 */
	private static final int SLOT_PADDING = 8;

	public LayerData(int x,int y,int w,int h){
		super(x, y, w, h);
	}
	
	@Override
	public void paint(Graphics g) {
		
	}
	
	/**
	 * 绘制该窗口内所有内容
	 * @param title 窗口标题图片对象
	 * @param players 玩家数组
	 * @param g 画笔对象
	 */
	public void paintAll(Image title,List<Player> players,Graphics g){
		g.drawImage(title, this.dx+PADDING, this.dy+PADDING, null);
		double nowPoint = (double)this.dto.getNowPoint();
		
		for (int i = 0; i < MAX_PLAYER; i++) {
			double dbPoint = (double)players.get(i).getPoint();
			double percent = nowPoint/dbPoint;
			String string = players.get(i).getName();
			percent = percent<1 ? percent:1;
			this.drawRect(0, DB_H+SLOT_PADDING+(SLOT_PADDING+SLOT_H)*i, percent, SLOT_H, 
					Integer.toString((int)dbPoint),string, Img.RECT, g);
		}
	}

}
