package ui;

import java.awt.Image;

import javax.swing.ImageIcon;

public class Img {

	/*
	 * 构造器私有化
	 */
	private Img(){}
	
	/*
	 * 得分标题图片
	 */
	public static Image POINT = new ImageIcon("graphics/string/point.png").getImage();
	
	/*
	 * 已消去行标题图片
	 */
	public static Image RMLINE = new ImageIcon("graphics/string/rmline.png").getImage();
	
	/*
	 * 版权信息图片
	 */
	public static Image SING = new ImageIcon("graphics/string/sign.png").getImage();
	
	/*
	 * 值槽显示图片
	 */
	public static Image RECT = new ImageIcon("graphics/window/rect.png").getImage();
	
	/*
	 * 本地记录标题图片
	 */
	public static Image DISK = new ImageIcon("graphics/string/disk.png").getImage();
	
	/*
	 * 数据库标题图片
	 */
	public static Image Database = new ImageIcon("graphics/string/db.png").getImage(); 
	
	/*
	 * 窗口图片
	 */
	public static Image WINDOW = new ImageIcon("graphics/window/Window.png").getImage();
	
	/**
	 * 数字图片  260*36
	 */
	public static final Image NUM = new ImageIcon("graphics/string/num.png").getImage();
	
	/*
	 * 游戏背景图片
	 */
	public static Image BG = new ImageIcon("graphics/background/bg02.jpg").getImage(); 
	
	/*
	 * 游戏方块图片
	 */
	public static Image ACT = new ImageIcon("graphics/game/rect.png").getImage();
	
	/**
	 * 等级标题图片
	 */
	public static final Image LEVEL = new ImageIcon("graphics/string/level.png").getImage();
	
	/**
	 * 游戏下落方块阴影图片
	 */
	public static final Image SHADOW = new ImageIcon("graphics/window/shadow.png").getImage();
	
	/**
	 * 设置按钮图片
	 */
	public static final ImageIcon CONFIG = new ImageIcon("graphics/string/config.png");
	
	/**
	 * 开始按钮图片
	 */
	public static final ImageIcon STRAT = new ImageIcon("graphics/string/start.png");
}
