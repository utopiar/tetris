package ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

public class LayerPoint extends Layer {
	
	/*
	 * 升一级需要消去的行数
	 */
	private static final int UP_RM_LINE = 20;
	
	/**
	 * 分数图片高度
	 */
	public static final int IMG_POINT_H = Img.POINT.getHeight(null);
	
	/**
	 * 值槽高度
	 */
	private static int VALUE_SLOT ;
	
	/**
	 * 公用起点X坐标
	 */
	protected static int comX;
	
	public LayerPoint(int x,int y,int w,int h){		
		super(x,y,w,h);
		comX = this.dx+PADDING;
		VALUE_SLOT = this.window_h - (PADDING<<2) - (IMG_POINT_H<<1);
	}
		
	public void paint(Graphics g){
		this.creatWindow(g);
		//显示分数标题
		g.drawImage(Img.POINT, comX, this.dy+PADDING,  null);
		int offset = Img.POINT.getWidth(null)+PADDING;
		this.drawNumber(this.window_w-offset, this.dx+offset,PADDING, this.dto.getNowPoint(), g);
		//显示消行标题
		g.drawImage(Img.RMLINE, comX, this.dy+(PADDING<<1)+IMG_POINT_H,  null);
		this.drawNumber(this.window_w-offset, this.dx+offset,(PADDING<<1)+IMG_POINT_H, this.dto.getNowRemoveLine(), g);
		//绘制值槽
		int nowRmLine = this.dto.getNowRemoveLine();
		double percent = (double)((nowRmLine%UP_RM_LINE)/ (double)UP_RM_LINE);
		this.drawRect(0, IMG_POINT_H+PADDING<<1,percent, VALUE_SLOT,"","下一级",Img.RECT, g);

	}
	
	//------------------------过时的方法---------------------------------
	
	/**
	 * 绘制值槽
	 */
	public void oldDrawRect(int mySlot,int maxSlot,String str,Graphics g){
		//绘制值槽背景
		g.setColor(Color.black);
		g.fillRect(comX, this.dy+(PADDING*3)+(IMG_POINT_H<<1), this.window_w-(PADDING<<1), VALUE_SLOT);
		//绘制值槽内边框
		g.setColor(Color.white);
		g.fillRect(comX+2, this.dy+(PADDING*3)+(IMG_POINT_H<<1)+2, this.window_w-(PADDING<<1)-4, VALUE_SLOT-4);
		g.setColor(Color.black);
		g.fillRect(comX+4, this.dy+(PADDING*3)+(IMG_POINT_H<<1)+4, this.window_w-(PADDING<<1)-8, VALUE_SLOT-8);
	    //计算值槽内径宽度		
		double per = (double)(mySlot%maxSlot)/(double)maxSlot;
		int myWidth = (int)(per * (this.window_w-(PADDING<<1)-8));
		g.setColor(getNowColor((double)(mySlot%maxSlot), (double)maxSlot));
		g.fillRect(comX +4, this.dy+(PADDING*3)+(IMG_POINT_H<<1)+4, myWidth, VALUE_SLOT-8);
		//绘制下一级标题
		g.setColor(Color.white);
		g.setFont(new Font("宋体", Font.BOLD, 20));
		g.drawString(str, comX +4, this.dy+(PADDING*3)+(IMG_POINT_H<<1)+4+22);
	}
	
	/**
	 * 值槽颜色改变
	 * @param hp
	 * @param maxHp
	 * @return
	 */
	public Color getNowColor(double hp,double maxHp){
		int colorR = 0;
		int colorG = 255;
		int colorB = 0;
		double hpHalf = maxHp /2;
		if(hp > hpHalf){
			colorR = 255 - (int)((hp-hpHalf)/(maxHp/2)*255);
			colorG = 255;
		}else {
			colorR = 255;
			colorG = (int)(hp /(maxHp / 2)*255);
		}
		return new Color(colorR,colorG,colorB);
	}
	

}
