package ui;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;

public class LayerBackground extends Layer {
	
	public static final List<Image> IMG_LIST = new ArrayList<Image>();
	
	public LayerBackground(int x,int y,int w,int h){
		
		super(x,y,w,h);
	}
	
	@Override
	public void paint(Graphics g){
		File file = new File("graphics/background");
		File[] files = file.listFiles();
		int imgNum = this.dto.getNowLevel()%files.length;
		g.drawImage(new ImageIcon(files[imgNum].getPath()).getImage(), 0, 0, this.window_w,this.window_h,null);
	}
}
