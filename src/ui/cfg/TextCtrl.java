package ui.cfg;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JTextField;

public class TextCtrl  extends JTextField{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int keyCode;
	
	private final String methodName ;

	public TextCtrl(int x,int y,int w,int h,String methodName){
		this.setBounds(x, y, w, h);
		this.methodName = methodName;
		this.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				setText(null);
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				keyCode = e.getKeyCode();
				String str = KeyEvent.getKeyText(keyCode);				
				setText(str);
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
			}
		});
	}

	public int getKeyCode() {
		return keyCode;
	}

	public void setKeyCode(int keyCode) {
		this.keyCode = keyCode;
		setText(KeyEvent.getKeyText(keyCode));
	}

	public String getMethodName() {
		return methodName;
	}
	
	
}
