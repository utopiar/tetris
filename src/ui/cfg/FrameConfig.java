package ui.cfg;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import util.FrameUtil;

public class FrameConfig extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JButton btnOk = new JButton("确定");
	
	private JButton btnCancel = new JButton("取消");
	
	private JButton btnUser = new JButton("应用");
	
	private TextCtrl[] textFields = new TextCtrl[8];
	
	private JLabel errMsg = new JLabel();
	
	private static final Image IMG_CTRL = new ImageIcon("data/psp1.png").getImage();
	
	private HashMap<String, Integer> keyActions = new HashMap<String, Integer>();
	
	private String[] methods = new String[]{
			"keyRight","keyUp","keyLeft","keyDown","keyFunLeft","keyFunUp","keyFunRight","keyFunDown"
	};
	
	private String PATH = "data/control.dat";
		
	public FrameConfig(){
		//设置窗口大小不可调整
		this.setResizable(false);
		this.setSize(620,335);
		//窗口居中显示
		FrameUtil.setFrameCenter(this);
		//设置布局管理器为“边界布局”
		this.setLayout(new BorderLayout());
		this.add(this.creatButtonPanel(),BorderLayout.SOUTH);
		this.add(this.creatMainPanel(),BorderLayout.CENTER);
		//TODO 测试
		this.setDefaultCloseOperation(3);
		this.setVisible(true);
	}

	/**
	 * 初始化设置文本域
	 */
	@SuppressWarnings("unchecked")
	public void initText(JPanel jp){
		int x = 0;
		int y = 50;		
		for (int i = 0; i < 4; i++) {
			this.textFields[i] = new TextCtrl(x, y+25*i, 60, 20,methods[i]);
			jp.add(this.textFields[i]);
		}
		for (int i = 4; i < 8; i++) {
			this.textFields[i] = new TextCtrl(x+550, y+25*(i-4), 60, 20,methods[i]);
			jp.add(this.textFields[i]);
		}
		try {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(PATH));
			keyActions = (HashMap<String, Integer>) ois.readObject();
			for (TextCtrl tc : textFields) {
				if(keyActions.containsKey(tc.getMethodName()))
				    tc.setKeyCode(keyActions.get(tc.getMethodName()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * 创建主面板（选项卡面板）
	 * @return
	 */
	private JTabbedPane creatMainPanel() {
		JTabbedPane jTabbedPane = new JTabbedPane();
		jTabbedPane.addTab("控制设置", this.creatControlPanel());
		jTabbedPane.addTab("皮肤设置", new JLabel("皮肤"));
		return jTabbedPane;
	}

	/**
	 * 玩家控制设置面板
	 * @return
	 */
	private JPanel creatControlPanel() {
		JPanel jp = new JPanel(){
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void paint(Graphics g){
				super.paint(g);
				g.drawImage(IMG_CTRL,0,0,null);
				
			}			
		};
		jp.setLayout(null);
		this.initText(jp);	
		return jp;
	}

	/**
	 * 创建按钮面板
	 * @return
	 */
	private JPanel creatButtonPanel() {
		JPanel jPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		errMsg.setForeground(Color.red);
		errMsg.setFont(new Font("微软雅黑",Font.BOLD,15));
		jPanel.add(this.errMsg);
		this.btnOk.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(writKeyCtrl())
				    setVisible(false);
			}
		});
		jPanel.add(this.btnOk);
		this.btnCancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		jPanel.add(this.btnCancel);
		this.btnUser.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				writKeyCtrl();
			}
		});
		jPanel.add(this.btnUser);
		return jPanel;
	}
	
	public boolean writKeyCtrl(){
		HashMap<Integer, String> tempMap = new HashMap<Integer, String>();
		for (int i = 0; i < textFields.length; i++) {
			int kc = textFields[i].getKeyCode();
			 if(0 == kc){
				errMsg.setText("按键为空");
				return false;
			}
			keyActions.put(methods[i],textFields[i].getKeyCode());	
			tempMap.put(textFields[i].getKeyCode(), methods[i]);
		}
		if(tempMap.size()!=8){
			errMsg.setText("按键重复");
			return false;
		}
		
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(PATH));
			oos.writeObject(keyActions);
			oos.close();
		} catch (Exception e) {
			e.printStackTrace();			
			errMsg.setText("e.getMessage()");
			return false;
		}
		errMsg.setText("");
		return true;
	}
	
	
	public static void main(String[] args){
		new FrameConfig();
	}
}
