package ui;

/**
 * @author Administrator
 * 功能：绘制窗口
 */

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import config.ConfigFrame;
import config.ConfigGame;
import dto.GameDto;

abstract public class Layer {

	
	/**
	 * 定义图像常量、其长度、宽度
	 */
	protected  static final int BORDER;
	
	/*
	 * 窗口图片宽度
	 */
	protected static int PIC_W = Img.WINDOW.getWidth(null);
	
	/*
	 * 窗口图片高度
	 */
	protected static int PIC_H = Img.WINDOW.getHeight(null);
	
	/*
	 * 内边距 
	 */
	protected  static final int PADDING;
	
	/**
	 * 数字图片中单个数字宽度
	 */
	private static final int IMG_W = Img.NUM.getWidth(null)/10;
	/**
	 * 数字图片的高度
	 */
	private static final int IMG_H = Img.NUM.getHeight(null);
	
	/**
	 * 静态代码块，引入XML配置文件中的参数
	 */
	static{
		ConfigFrame cfgF = ConfigGame.getConfigFrame();
	    BORDER = cfgF.getBorder();
	    PADDING = cfgF.getPadding();
	}
	
	/**
	 * 窗口左上角x坐标
	 */
	protected int dx;
	
	/**
	 * 窗口左上角y坐标
	 */
	protected int dy;
	
	/**
	 * 窗口宽度
	 */
	protected int window_w;
	
	/**
	 * 窗口高度
	 */
	protected int window_h;
	

	
	protected Layer(int x,int y,int w,int h){
		
		this.dx = x;
		this.dy = y;
		this.window_w = w;
		this.window_h = h;
	}
	
	/**
	 * 游戏数据
	 */
	protected GameDto dto;
	
	/*
	 * 将游戏数据载体Dto对象绑定到游戏层
	 */
	public void setDto (GameDto dto){
		this.dto = dto;
	}
	
	//绘制窗口
	protected void creatWindow(Graphics g){

		//左上
		g.drawImage(Img.WINDOW, dx, dy, dx+BORDER, dy+BORDER, 0, 0, BORDER, BORDER, null);
		//中上
		g.drawImage(Img.WINDOW, dx+BORDER, dy, window_w-BORDER+dx, dy+BORDER, BORDER, 0 ,PIC_W-BORDER, BORDER, null);
		//右上
		g.drawImage(Img.WINDOW, window_w-BORDER+dx, dy, window_w+dx, dy+BORDER, PIC_W-BORDER, 0, PIC_W, BORDER, null);
		//左中
		g.drawImage(Img.WINDOW, dx, dy+BORDER, dx+BORDER, window_h+dy-BORDER, 0, BORDER, BORDER, PIC_H-BORDER, null);
		//中
		g.drawImage(Img.WINDOW, dx+BORDER, dy+BORDER, window_w+dx-BORDER, window_h+dy-BORDER, BORDER, BORDER, PIC_W-BORDER, PIC_H-BORDER, null);
		//右中
		g.drawImage(Img.WINDOW, window_w+dx-BORDER, dy+BORDER, window_w+dx, window_h+dy-BORDER, PIC_W-BORDER, BORDER, PIC_W, PIC_H-BORDER, null);
		//左下
		g.drawImage(Img.WINDOW, dx, window_h+dy-BORDER, dx+BORDER, window_h+dy, 0, PIC_H-BORDER, BORDER, PIC_H, null);
		//中下
		g.drawImage(Img.WINDOW, dx+BORDER, window_h+dy-BORDER, dx+window_w-BORDER, window_h+dy, BORDER, PIC_H-BORDER, PIC_W-BORDER, PIC_H, null);
		//右下
		g.drawImage(Img.WINDOW, window_w+dx-BORDER, window_h+dy-BORDER, window_w+dx, window_h+dy, PIC_W-BORDER, PIC_H-BORDER, PIC_W, PIC_H, null);
	}
	
	/**
	 * 绘制数字
	 * @param w    窗口实际宽度
	 * @param x    x轴方向偏移量
	 * @param y    Y轴方向偏移量
	 * @param num  需要绘制的数字
	 * @param g    笔刷对象
	 */
	public void drawNumber(int w,int x,int y,int num,Graphics g){
		String str = Integer.toString(num);
		int length = str.length();
		for (int i = 0; i < length; i++) {
			int temp = str.charAt(i) - '0';	
			int center = w - IMG_W*length >>1;
			g.drawImage(Img.NUM, 
				x + i * IMG_W + center, dy + y, 
				x + IMG_W + i * IMG_W + center, 
				dy + IMG_H + y, 
				temp * IMG_W, 
				0, 
				(temp + 1) * IMG_W,
				IMG_H, 
				null);
		}
	}
	
	/**
	 * 正中绘图
	 */
	public void drawImageAtCenter(Image img,Graphics g){
		int imgW = img.getWidth(null);
		int imgH = img.getHeight(null);
		g.drawImage(img, dx+(window_w - imgW >>1), dy + (window_h - imgH >>1), null);
	}
	
	/**
	 * 绘制值槽（使用图片）
	 * @param x X轴方向偏移量（不用再将起点坐标和边框值加进去）
	 * @param y Y轴方向偏移量（不用再将起点坐标和边框值加进去）
	 * @param percent 值槽百分比
	 * @param height 值槽高度
	 * @param num 需要绘制的数字（字符串形式）
	 * @param str 值槽标题
	 * @param img 值槽图片
	 * @param g 画笔对象
	 */
	public void drawRect(int x,int y,double percent,int height, String num,String str,Image img,Graphics g){
		int allX = dx+PADDING;
		int allY = dy+PADDING;
		//绘制背景
		g.setColor(Color.gray);
		g.fillRect(allX+x, allY+y, this.window_w-(PADDING<<1), height);
		//绘制边框
		g.setColor(Color.white);
		g.fillRect(allX+x+2, allY+y+2, this.window_w-(PADDING<<1)-4, height-4);
		//计算值槽内径宽度		
		g.setColor(Color.black);
		g.fillRect(allX+x+4, allY+y+4, this.window_w-(PADDING<<1)-8, height-8);
		int myWidth = (int)(percent * (this.window_w-(PADDING<<1)-8));
		int rectX = (int)(percent * Img.RECT.getWidth(null)-1);
		g.drawImage(Img.RECT, allX+x+4, allY+y+4, allX+x+4+myWidth, allY+y+4+height-8, rectX, 0, rectX+1, Img.RECT.getHeight(null), null);
		//绘制下一级标题
		g.setColor(Color.white);
		g.setFont(new Font("宋体", Font.BOLD, 20));
		g.drawString(str, allX+x+4, allY+y+4+22);
		num = num.equals("0") ? null : num;
		if(!(num == null)){
			for (int i = num.length(); i > 0; i--) {
				char temp = num.charAt(i-1);
				g.drawString(String.valueOf(temp), allX+x+4+280-11*(num.length()-i), allY+y+4+22);
			}
			
		}		
	}
	
	/**
	 * 抽象方法
	 * @param g 画笔对象
	 */
	abstract public void paint(Graphics g);
}
