package ui;

import java.awt.Graphics;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import config.ConfigFrame;
import config.ConfigGame;
import config.ConfigLayer;
import control.ControlPlayer;
import dto.GameDto;

public class JPanelGame extends JPanel{

	/**
	 * 保证程序兼容性
	 */
	private static final long serialVersionUID = 1L;
	
	private GameDto dto;
	
	private List<Layer> layers = null;
	
	public JPanelGame(GameDto dto){
		
		this.dto = dto;
		//初始化游戏绘制层
		initComponent();
		//初始化组件
		initLayer();

	}
	
	/**
	 * 安装玩家控制器
	 * @param control
	 */
	public void setPlayerControl(ControlPlayer control){
		this.addKeyListener(control);
	}
	
	/**
	 * 初始化组件
	 */
	public void initComponent(){
		
	}
	
	/**
	 * 初始化游戏绘制层
	 */
	public void initLayer(){
		//获得游戏配置
		//旧的方法
		//GameConfig cfg = ConfigFactory.getGameConfig();
		//重新构造config类后的方法
		ConfigFrame cfgF = ConfigGame.getConfigFrame();
		//获得层配置
		List<ConfigLayer> layersCfg = cfgF.getLayersConfig();
		//创建游戏层数组
		layers = new ArrayList<Layer>(layersCfg.size());
		//创建所有层对象
		for(ConfigLayer layerCfg : layersCfg){			
			try {
				//获得类对象
				Class<?> cls = Class.forName(layerCfg.getClassName());
				Constructor<?> ctr = cls.getConstructor(int.class,int.class,int.class,int.class);
				Layer l = (Layer)ctr.newInstance(
						layerCfg.getX(),layerCfg.getY(),layerCfg.getW(),layerCfg.getH()
				);
				l.setDto(dto);
				layers.add(l);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void paintComponent(Graphics g){
	
		super.paintComponent(g);
		//绘制游戏界面
		for(int i=0;i<layers.size();i++){
			layers.get(i).paint(g);
		}
		//返回焦点
		this.requestFocus();
		
	}
	
}
