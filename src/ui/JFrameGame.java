package ui;

import javax.swing.JButton;
import javax.swing.JFrame;

import util.FrameUtil;

import config.ConfigFrame;
import config.ConfigGame;

public class JFrameGame extends JFrame{
	
	/**
	 * 保证兼容性
	 */
	private static final long serialVersionUID = 1L;
	
	private JButton btnConfig = new JButton();
	
	//private JButton btnStart = new JButton();

	public JFrameGame(JPanelGame panelGame){
		//重新构造config后的方法
		ConfigFrame cfgF = ConfigGame.getConfigFrame();
		//设置标题
		this.setTitle(cfgF.getTitle());
		//设置默认关闭属性（程序结束）
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//设置窗口大小
		this.setSize(cfgF.getWidth(), cfgF.getHeight());
		//不允许用户改变窗口大小
		this.setResizable(false);
		FrameUtil.setFrameCenter(this);
		this.setContentPane(panelGame);
		btnConfig.setIcon(Img.CONFIG);
		btnConfig.setBounds(964, 39, 150, 75);
		this.add(btnConfig);
		//默认窗口显示
		this.setVisible(true);
	}
}
