package ui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

public class LayerGame extends Layer {		
	
	/**
	 * 左位移偏移量
	 */
    private static int SIZE_ROL = 5;
	
	public LayerGame(int x,int y,int w,int h){
		
		super(x,y,w,h);
	}
	
	public void getCoord (Graphics g,boolean bool){
		if(!bool){
			return;
		}
		Point[] points = this.dto.getGameAct().getActPoints();
		int leftX = 9;
		int rightY = 0;
		for (Point point : points) {
			leftX = point.x < leftX ? point.x : leftX;
			rightY = point.x > rightY ? point.x : rightY;
		}
		g.setColor(Color.blue);
		//g.fillRect(this.dx+BORDER+(leftX<<SIZE_ROL), this.dy+BORDER, rightY-leftX+1<<SIZE_ROL, 18<<SIZE_ROL);
	    g.drawImage(Img.SHADOW, this.dx+BORDER+(leftX<<SIZE_ROL), this.dy+BORDER, rightY-leftX+1<<SIZE_ROL, 18<<SIZE_ROL, null);
	}
	
	public void paint(Graphics g){
		this.creatWindow(g);
		Point[] actPoints = this.dto.getGameAct().getActPoints();
		int typeCode = this.dto.getGameAct().getTypeCode();
		//绘制方块
		for(Point actPoint : actPoints){
			drawActByPaint(actPoint.x, actPoint.y, typeCode+1, g);
		}
		//绘制地图
		int imgIdx = this.dto.getNowLevel();
		imgIdx = imgIdx == 0 ? 0 : (imgIdx - 1)%7+1;
		boolean[][] map = this.dto.getGameMap();
		for(int x=0 ; x<map.length ;x++){
			for(int y=0 ;y<map[x].length; y++){
				if(map[x][y])
					drawActByPaint(x, y, imgIdx, g);
			}
		}
		this.getCoord(g,true);
	}
	
	
	/**
	 * 绘制方块
	 * @param x 相对于游戏区域内的x坐标
	 * @param y 相对于游戏区域内的y坐标
	 * @param imgIdx 截取方块图片的位置
	 * @param g Graphics对象
	 */
	protected void drawActByPaint(int x,int y,int imgIdx,Graphics g){
		g.drawImage(Img.ACT, 
				dx+(x<<SIZE_ROL)+BORDER, 
				dy+(y<<SIZE_ROL)+BORDER, 
				dx+(x+1<<SIZE_ROL)+BORDER, 
				dy+(y+1<<SIZE_ROL)+BORDER, 
				imgIdx<<SIZE_ROL, 0, imgIdx+1<<SIZE_ROL, 32,  null);
	}

}